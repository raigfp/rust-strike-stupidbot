import asyncio
import websockets
import time

import game
import player

async def run():
    async with websockets.connect('ws://localhost:3012') as websocket:
        # shit
        await websocket.send('{"look": {"x": 0, "y": 100}}')

        while True:
            time.sleep(1)

            game.data = await websocket.recv()
            print("< {}".format(game.data))

            action = player.play()
            if action:
                await websocket.send(action)
                print("> {}".format(action))


asyncio.get_event_loop().run_until_complete(run())

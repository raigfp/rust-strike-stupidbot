import json

state = {
    'is_forward': False,
    'is_backward': False,
    'is_left': False,
    'is_right': False,
    'is_attack': False,
}


def move_forward(status):
    state['is_forward'] = status
    return json.dumps({'forward': status})


def move_backward(status):
    state['is_backward'] = status
    return json.dumps({'backward': status})


def move_left(status):
    state['is_left'] = status
    return json.dumps({'left': status})


def move_right(status):
    state['is_right'] = status
    return json.dumps({'right': status})


def attack(status):
    state['is_attack'] = status
    return json.dumps({'attack': status})


def play():
    action = False

    if not state['is_attack']:
        action = attack(True)

    return action
